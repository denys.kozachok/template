package com.template.ui;

import com.codeborne.selenide.Selenide;
import com.template.ui.steps.AboutUsSteps;
import com.template.ui.steps.ContactsSteps;
import com.template.ui.steps.MainSteps;
import org.testng.annotations.Test;

public class UiTest extends BaseTest {

    MainSteps mainPageSteps = new MainSteps();
    AboutUsSteps aboutUsPageSteps = new AboutUsSteps();
    ContactsSteps contactsPageSteps = new ContactsSteps();
    @Test(description = "Verify success message after adding item to cart")
    public void openTest() {
        aboutUsPageSteps.open();
        mainPageSteps.open();
        contactsPageSteps.open();
    }
}