package com.template.ui;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.testng.TextReport;
import com.template.ui.helpers.listener.AllureScreenShooter;
import com.template.ui.helpers.listener.Listener;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;

import static com.template.ui.helpers.properties.UrlLinksProp.MAIN_URL;

@Listeners({AllureScreenShooter.class, TextReport.class, Listener.class})
public class BaseTest {


    @BeforeTest
    public void setup(){
        Configuration.browser =  "chrome";
        Configuration.baseUrl = MAIN_URL;
        Configuration.browserSize = "1920x1080";
        Configuration.timeout =  5000;
        Configuration.reportsFolder = "target/test-result/reports";
        Configuration.holdBrowserOpen = true;
    }
}

