package com.template.ui.steps;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Step;

public class AboutUsSteps {

    @Step("open about Us page")
    public void open(){
        Selenide.open("about/");
    }
}
