package com.template.ui.steps;

import com.codeborne.selenide.Selenide;
import com.template.ui.pages.AddedToCartPage;
import com.template.ui.pages.ItemPage;
import com.template.ui.pages.MainPage;
import com.template.ui.pages.SearchPage;
import io.qameta.allure.Step;

public class ContactsSteps {

    @Step
    public void open() {
        Selenide.open("contacts/");
    }

}
