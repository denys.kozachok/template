package com.template.ui.steps;

import com.codeborne.selenide.Selenide;
import com.template.ui.pages.AddedToCartPage;
import com.template.ui.pages.ItemPage;
import com.template.ui.pages.MainPage;
import com.template.ui.pages.SearchPage;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.back;

public class MainSteps {
    private static MainPage mainPage = new MainPage();
    private static SearchPage searchPage = new SearchPage();
    private static ItemPage itemPage = new ItemPage();
    private static AddedToCartPage addedToCartPage = new AddedToCartPage();

    @Step
    public void open(){
        Selenide.open("/");
    }
}
